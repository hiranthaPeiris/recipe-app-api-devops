resource "aws_vpc" "main" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-VPC" })
  )
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-IGW" })
  )
}

###############################################
# Public subnets - Inbound and Outbound subnets
###############################################

resource "aws_subnet" "public_a" {
  cidr_block              = "10.1.1.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.main.id
  availability_zone       = "${data.aws_region.current.name}a"

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-public-a" })
  )
}

resource "aws_route_table" "public-rtb" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-public-rtb" })
  )
}

resource "aws_route_table_association" "public_a" {
  subnet_id      = aws_subnet.public_a.id
  route_table_id = aws_route_table.public-rtb.id
}

resource "aws_route" "public_internet_access_a" {
  route_table_id         = aws_route_table.public-rtb.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

# resource "aws_eip" "public_a" {
#   vpc = true

#   tags = merge(
#     local.common_tags,
#     tomap({ "Name" = "${local.prefix}-public-a" })
#   )
# }

# resource "aws_nat_gateway" "public-ngw" {
#   allocation_id = aws_eip.public_a.id
#   subnet_id     = aws_subnet.public_a.id

#   tags = merge(
#     local.common_tags,
#     tomap({ "Name" = "${local.prefix}-public-a-ngw" })
#   )
# }

resource "aws_subnet" "public_b" {
  cidr_block              = "10.1.2.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.main.id
  availability_zone       = "${data.aws_region.current.name}b"

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-public-b" })
  )
}

# resource "aws_route_table" "public_b" {
#   vpc_id = aws_vpc.main.id

#   tags = merge(
#     local.common_tags,
#     tomap({ "Name" = "${local.prefix}-public-b" })
#   )
# }

resource "aws_route_table_association" "public_b" {
  subnet_id      = aws_subnet.public_b.id
  route_table_id = aws_route_table.public-rtb.id
}

# resource "aws_route" "public_internet_access_b" {
#   route_table_id         = aws_route_table.public-rtb.id
#   destination_cidr_block = "0.0.0.0/0"
#   gateway_id             = aws_internet_gateway.main.id
# }

# resource "aws_eip" "public_b" {
#   vpc = true
#   tags = merge(
#     local.common_tags,
#     tomap({ "Name" = "${local.prefix}-public-b" })
#   )
# }

# resource "aws_nat_gateway" "public_b" {
#   allocation_id = aws_eip.public_b.id
#   subnet_id     = aws_subnet.public_b.id

#   tags = merge(
#     local.common_tags,
#     tomap({ "Name" = "${local.prefix}-public-b" })
#   )
# }


###############################################
# Private subnets - Outbound internet access only
###############################################

# resource "aws_subnet" "private_a" {
#   cidr_block        = "10.1.10.0/24"
#   vpc_id            = aws_vpc.main.id
#   availability_zone = "${data.aws_region.current.name}a"

#   tags = merge(
#     local.common_tags,
#     tomap({ "Name" = "${local.prefix}-private-a" })
#   )
# }

# resource "aws_route_table" "private_rtb" {
#   vpc_id = aws_vpc.main.id

#   tags = merge(
#     local.common_tags,
#     tomap({ "Name" = "${local.prefix}-private-rtb" })
#   )
# }

# resource "aws_route_table_association" "private_a" {
#   subnet_id      = aws_subnet.private_a.id
#   route_table_id = aws_route_table.private_rtb.id
# }

# resource "aws_route" "private_a_internet_out" {
#   route_table_id         = aws_route_table.private_rtb.id
#   nat_gateway_id         = aws_nat_gateway.public-ngw.id
#   destination_cidr_block = "0.0.0.0/0"
# }

# resource "aws_subnet" "private_b" {
#   cidr_block        = "10.1.11.0/24"
#   vpc_id            = aws_vpc.main.id
#   availability_zone = "${data.aws_region.current.name}b"

#   tags = merge(
#     local.common_tags,
#     tomap({ "Name" = "${local.prefix}-private-b" })
#   )

# }

# resource "aws_route_table_association" "private_b" {
#   subnet_id      = aws_subnet.private_b.id
#   route_table_id = aws_route_table.private_rtb.id
# }

# # output "aws_subnet_public_a" {
# #   value = aws_subnet.public_a.id
# # }