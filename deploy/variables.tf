variable "vpc" {
  type    = any
  default = "vpc-0701b49de01d58a63"
}

variable "sg_pub_id" {
  type    = string
  default = "sg-0c96a4fed6314778a"
}

variable "prefix" {
  default = "TerraRND"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "db_username" {
  description = "Username of the AWS RDS database"
}

variable "db_password" {
  description = "passwd for the RDS"
}