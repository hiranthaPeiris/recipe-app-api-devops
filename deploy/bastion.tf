# module "network" {
#   source = "./network.tf"
# }

data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

resource "aws_security_group" "inside" {
  description = "Allow access to rds"
  name        = "${local.prefix}-ec2-inbound-access"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "SSH from outside"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-inside-main" })
  )
}

resource "aws_instance" "bastion" {
  ami                         = data.aws_ami.amazon_linux.id
  associate_public_ip_address = true
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.public_a.id
  vpc_security_group_ids      = [aws_security_group.inside.id]

  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-bastion" })
  )
}